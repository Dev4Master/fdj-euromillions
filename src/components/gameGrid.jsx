import React, { Component } from "react";
import config from "../config.json";
import { getNumbers, getStars, lockNumberItem } from "../services/gridService";
import { buildPrices, getMaxSelection } from "../services/utils";

import GridTitle from "../common/gridTitle";
import BetPrice from "../common/betPrice";
import GridNumbers from "./gridNumbers";
import GridStars from "./gridStars";

class GameGrid extends Component {
  state = {
    betValue: 0,
    numbers: [],
    stars: [],
    pricesList: [],
    selection: { numbers: 0, stars: 0 }
  };

  getBetValue = () => {
    const { selection } = this.state;
    const key = `${selection.numbers}${selection.stars}`;
    const price = this.state.pricesList.find(p => p.code === key);

    return price ? price.value * 0.01 : 0;
  };

  componentDidMount() {
    this.setState({
      numbers: getNumbers(),
      stars: getStars(),
      pricesList: buildPrices(config.multiples)
    });
  }

  componentDidUpdate(prevProps, prevState) {
    // Typical usage (don't forget to compare props):
    if (this.state.selection !== prevState.selection) {
      const { selection, numbers, stars } = this.state;

      const numbersList = lockNumberItem(
        selection.numbers,
        numbers,
        getMaxSelection(selection.stars, "star")
      );
      const starsList = lockNumberItem(
        selection.stars,
        stars,
        getMaxSelection(selection.numbers, "number")
      );

      console.table(numbersList);
      console.table(starsList);

      this.setState({
        numbers: numbersList,
        stars: starsList,
        betValue: this.getBetValue()
      });
    }
  }

  handleItemCheck = (item, itemSource) => {
    if (!item.checkable) return;

    const itemsList = [...this.state[itemSource]];
    const index = itemsList.findIndex(i => i._id === item._id);
    itemsList[index] = { ...itemsList[index] };
    itemsList[index].checked = !itemsList[index].checked;

    this.setState({ [itemSource]: itemsList });
    const selection = { ...this.state.selection };
    selection[itemSource] = itemsList.filter(i => i.checked).length;
    this.setState({ selection });
  };

  getStarPic() {
    return (
      <svg xmlSpace="preserve" viewBox="92.5 0 583.508 546.833" x="0" y="0">
        <path
          className="star-body"
          d="M529.754,531.833c-6.497,0-13.093-1.576-19.075-4.558l-126.424-64.621L257.76,527.311
              c-5.709,2.915-12.46,4.522-19.006,4.522c-8.924,0-17.058-2.551-24.175-7.582c-12.74-8.979-19.23-24.603-16.552-39.817
              l24.065-136.605l-102.018-96.714c-11.284-10.677-15.432-27.057-10.578-41.74c5.034-14.979,17.509-25.293,33.347-27.521
              l141.257-19.928l63.036-124.483C354.228,23.627,368.401,15,384.087,15c15.93,0,29.877,8.482,37.327,22.694l62.994,124.399
              l141.25,19.926c15.729,2.212,28.507,12.693,33.354,27.354c4.854,14.683,0.706,31.063-10.567,41.73l-102.028,96.724l24.065,136.607
              c2.673,15.188-3.792,30.786-16.473,39.76C546.737,529.192,538.352,531.833,529.754,531.833z"
        />
      </svg>
    );
  }

  render() {
    const { betValue, stars, numbers } = this.state;
    return (
      <div className="main-content">
        <div className="grid-header">
          <GridTitle />
          <BetPrice betValue={betValue} />
        </div>
        <div className="grid-container">
          <GridNumbers
            numbersList={numbers}
            onToggleChecked={this.handleItemCheck}
          />
          <GridStars
            starsList={stars}
            onToggleChecked={this.handleItemCheck}
            svgPic={this.getStarPic()}
          />
        </div>
      </div>
    );
  }
}

export default GameGrid;
