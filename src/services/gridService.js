import _ from 'lodash';
import {
   getMaxSelection
} from './utils';

const numbers = _.range(1, 51);
const stars = _.range(1, 13);

export function getNumbers() {
   return numbers.map(n => {
      return {
         _id: n,
         value: n,
         checkable: true
      }
   });
}

export function getStars() {
   return stars.map(s => {
      return {
         _id: s,
         value: s,
         checkable: true
      }
   });
}

export function lockNumberItem(selectionItems, itemsList, max) {
   console.log('max', max);
   const items = [...itemsList];
   if (max === selectionItems) {
      items.forEach(n => {
         if (n.checked !== true) n.checkable = false;
      });
   } else if (max > selectionItems) {
      items.forEach(n => {
         if (n.checked !== true) n.checkable = true;
      });
   }

   return items;
}