import React, { Component } from "react";
import "./App.css";
import GameGrid from "./components/gameGrid";

class App extends Component {
  render() {
    return (
      <div className="App">
        <GameGrid />
      </div>
    );
  }
}

export default App;
