import React from "react";
import NumberItem from "./numberItem";

const GridNumbers = props => {
  const { numbersList, onToggleChecked } = props;
  return (
    <div className="numbers-grid">
      {numbersList.map(num => (
        <NumberItem
          key={num._id}
          numberItem={num}
          itemClass="number-container label-round label-primary"
          checkedClass="checked-number"
          onClick={() => onToggleChecked(num, "numbers")}
        />
      ))}
    </div>
  );
};

export default GridNumbers;
