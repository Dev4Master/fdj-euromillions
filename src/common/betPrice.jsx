import React from "react";

const BetPrice = props => {
  return <div className="bet-content">Mise totale {props.betValue} €</div>;
};

export default BetPrice;
