import React from "react";
import NumberItem from "./numberItem";

const GridStars = props => {
  const { starsList, onToggleChecked, svgPic } = props;
  return (
    <div className="stars-grid">
      {starsList.map(star => (
        <NumberItem
          key={star._id}
          numberItem={star}
          itemClass="number-container label-star label-secondary"
          checkedClass="checked-star"
          svgPic={svgPic}
          onClick={() => onToggleChecked(star, "stars")}
        />
      ))}
    </div>
  );
};

export default GridStars;
