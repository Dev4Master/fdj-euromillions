export function buildPrices(data) {
  const prices = data.map(m => {
    return {
      code: `${m.pattern[0]}${m.pattern[1]}`,
      value: m.cost.value
    };
  });

  return prices;
}

export function getMaxSelection(selection, type) {
  let checking = false;
  if (type === 'number') {
    switch (selection) {
      case 5:
      case 6:
        checking = 12;
        break;
      case 7:
        checking = 6;
        break;
      case 8:
        checking = 4;
        break;
      case 9:
        checking = 3;
        break;
      case 10:
        checking = 2;
        break;
      default:
        break;
    }
  } else {
    switch (selection) {
      case 2:
        checking = 10;
        break;
      case 3:
        checking = 9;
        break;
      case 4:
        checking = 8;
        break;
      case 5:
      case 6:
        checking = 7;
        break;
      case 7:
      case 8:
      case 9:
      case 10:
      case 11:
      case 12:
        checking = 6;
        break;
      default:
        break;
    }
  }

  return checking;
}