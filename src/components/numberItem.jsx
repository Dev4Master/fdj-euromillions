import React from "react";

const NumberItem = props => {
  const { numberItem, itemClass, svgPic, checkedClass } = props;
  let classes = itemClass + " ";
  numberItem.checkable ? (classes += "checkable") : (classes += "disabled");
  if (numberItem.checked) classes += " " + checkedClass;

  return (
    <div className={classes}>
      <span onClick={props.onClick} className="number">
        {numberItem.value}
      </span>
      if (svgPic) {svgPic}
    </div>
  );
};

export default NumberItem;
